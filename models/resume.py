from tempfile import NamedTemporaryFile
class Resume:
	@classmethod
	def insertResume(self, resume_file):
		"""
			There should only be one row per user
		"""
		rows = db(db.Resumes.user_id == auth.user_id).select()
		if len(rows) > 0:
			row = rows[0]
			row.update_record(resume = resume_file)
		else:
			row_data = {'user_id':auth.user_id, 'resume':resume_file}
			row_id = db.Resumes.insert(**row_data)
			assert row_id is not None
	def compile_resumes(self):
		resume_rows = db(db.Resumes).select()
		"""
		We have to take the rows, and sort them by their last name (and then first name)
		Since sorted in python is stable, we can do this by sorting by first name, and then last name.

		We have to map the user id in the Resumes table back to the auth_user table, and collect the first and last names.
		"""
		sorted_resume_rows_first_name = sorted(resume_rows, key = lambda row: db(db.auth_user).select(id == row.user_id).first().first_name))
		sorted_resume_rows = sorted(sorted_resume_rows_first_name, db(db.auth_user).select(id == row.user_id).first().last_name))
		resume_latex_text = "\usepackage{pdfpages} \n"
		for row in sorted_resume_rows:
			user_id = int(row.user_id)
			filename = row.resume
			#example: \includepdf[pages={1}]{myfile.pdf}
			resume_latex_text += "\includepdf[pages={-}]{" + filename + "} \newpage \n "
		temp_text_file_name = NamedTemporaryFile()
		with open(temp_text_file_name, 'w') as f:
			f.write(resume_latex_text)
		temp_pdf_name = NamedTemporaryFile()
		#have LaTeX render the document
		exec('pdflatex -jobname=' + temp_pdf_name + " " + temp_text_file_name)
		"""
		We will need to create a row in the Books table, and save the document to that (as a blob). Then, we will pass the row id back
		and another function can send the file to the browser for download. 
		"""
