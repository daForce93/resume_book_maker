response.app_version = '0.0.1'
response.generic_patterns = ['*.json']


db.define_table('Resumes',
	Field('user_id', db.auth_user), 
	Field('resume', 'upload')
	)

db.define_table('Books',
	Field('book', 'blob')
	)